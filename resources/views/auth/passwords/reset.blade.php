@extends('base.base_layout', [
'header_anonymous'  => 1,
'header_auth'       => 0,
'menu_main'         => 0,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div class="container" style="margin-top:30px">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel">
            <div class="panel-heading"><h3 class="panel-title"><strong>Recuperar Contraseña </strong></h3>
                <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="/">Iniciar Sesión</a></div>
            </div>

            <div class="panel-body">
                <form role="form" action="/password/reset" method="POST">
                    @if($errors->first("password") || $errors->first("email"))
                    <div class="alert alert-danger">
                        <a class="close" data-dismiss="alert" href="#">×</a>
                        @if($errors->first("password"))
                        {!! $errors->first("password") !!}
                        @endif
                        @if($errors->first("email"))
                        {!! $errors->first("email") !!}
                        @endif
                    </div>
                    @endif
                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="text" class="form-control" name="email" value="" placeholder="Email">                                        
                    </div>

                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="Nueva Contraseña">
                    </div>
                    <div style="margin-bottom: 12px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password_confirmation" placeholder="Repetir Contraseña">
                    </div>
                    <input id="token" type="hidden" name="token" value="{{ $token }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Iniciar Sesión</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop