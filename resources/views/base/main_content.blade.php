@if(isset($aside_left) && $aside_left)
<aside class="aside-left">
    @yield('aside_left')
</aside>
@endif
@yield('content')
@if(isset($aside_right) && $aside_right)
<aside class="aside_right">
    @yield('aside_right')
</aside>
@endif
@if(isset($layout_bottom) && $layout_bottom)
<div class="layout_bottom">
    @if(isset($layout_bottom_a) && $layout_bottom_a)
    <div class="layout_bottom_a">
        @yield('layout_bottom_a')
    </div>
    @endif
    @if(isset($layout_bottom_b) && $layout_bottom_b)
    <div class="layout_bottom_b">
        @yield('layout_bottom_b')
    </div>
    @endif
    @if(isset($layout_bottom_c) && $layout_bottom_c)
    <div class="layout_bottom_c">
        @yield('layout_bottom_c')
    </div>
    @endif
</div>
@endif