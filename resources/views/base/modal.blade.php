@if(isset($modal_count) && $modal_count > 0)
@for ($i = 1; $i <= $modal_count; $i++)
<section id="modal_{{ $i }}" class="modal @yield('modal_class_' . $i)">
    <div class="modal__box @yield('modal_box_class_' . $i) js-modal-size">
        <a href="#" title="Cerrar" class="btn-close ico-close js-modal-close"><span class="hidden">Cerrar </span></a>
        @yield('modal_content_' . $i)
    </div>
</section>
@endfor
@endif