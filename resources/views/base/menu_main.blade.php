@if(isset($menu_main) && $menu_main)
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            @if (Auth::user()->hasRole('admin'))
            <li>
                <a href="#"><i class="fa fa-user fa-fw"></i> Usuarios<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/user">Listado</a>
                    </li>
                    <li>
                        <a href="/user/create">Crear Nuevo</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="/settings"><i class="fa fa-gear fa-fw"></i> Configuración</a>
            </li>
            @elseif(Auth::user()->hasRole('operador'))
            <li>
                <a href="/picking/request"><i class="fa fa-truck fa-fw"></i> Solicitar Picking<span class="fa arrow"></span></a>
                
            </li>
            <li>
                <a href="/picking"><i class="fa fa-truck fa-fw"></i> Listado Picking<span class="fa arrow"></span></a>
            </li>
            @endif
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
@endif