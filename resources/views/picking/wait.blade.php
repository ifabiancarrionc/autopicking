@extends('base.base_layout', [
'header_anonymous'  => 0,
'header_auth'       => 1,
'menu_main'         => 1,
'aside_left'        => 0,
'aside_right'       => 0,
'layout_bottom'     => 0,
'layout_bottom_a'   => 0,
'layout_bottom_b'   => 0,
'layout_bottom_c'   => 0,
'modal_count'       => 0,
])
@section('class-html', '')
@section('title', 'Login')
@section('description-meta', '')
@section('aditional-meta')
<!-- Add meta tags to this page -->
@stop
@section('aditional-css')
<!-- Add link tags to this page -->
@stop
@section('class-body', 'page-contact')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Solicitar Picking</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <!-- /.panel-heading -->
            Un carro picking va a tu ubicación
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@stop
@section('aditional-js')
<<script>
    function showPosition(position) {
        $('#x_coordinate').val(position.coords.latitude);
        $('#y_coordinate').val(position.coords.longitude);
    }
    $(document).ready(function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $('.position').html("No se ha encontrado tu ubicación.");
        }
    })
</script>
@stop
<!-- If this page has the variable modal_count > 0,
then each modal content is identified with a number (#)-->
@section('modal_content_#')
<!-- Content of modal number # in this page -->
@stop