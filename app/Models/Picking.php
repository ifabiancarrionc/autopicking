<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picking extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'picking';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code','speed','x_cordinate','y_cordinate'];

    public function responses() {
        return $this->hasMany('App\Models\Response');
    }
    public function status() {
        return $this->hasMany('App\Models\Status');
    }
}