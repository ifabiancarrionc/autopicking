<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'request';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['current', 'x_coordinate', 'y_coordinate', 'user_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }
    public function response() {
        return $this->hasOne('App\Models\Response');
    }
}