<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'area';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','x_cordinate','y_cordinate'];

    public function users() {
        return $this->hasMany('App\User');
    }
    public function shipments() {
        return $this->hasMany('App\Models\Shipment');
    }
}