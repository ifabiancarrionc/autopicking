<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'response';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['estimated_time', 'request_id', 'picking_id'];

    public function request() {
        return $this->belongsTo('App\Models\Request');
    }
    public function picking() {
        return $this->belongsTo('App\Models\Picking');
    }
    public function shipment() {
        return $this->hasOne('App\Models\Shipment');
    }
}