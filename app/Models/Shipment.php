<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description','estimated_time', 'complete'];

    public function response() {
        return $this->belongsTo('App\Models\Response');
    }
    public function area() {
        return $this->belongsTo('App\Models\Area');
    }
}