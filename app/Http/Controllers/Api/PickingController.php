<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Picking;
use \Auth;

class PickingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {
        $picking = Picking::where('code', '=', $request->get('code'))
                ->first();
        if (!$picking) {
            $picking = new Picking();
            $picking->code = $request->get('code');
            $picking->speed = '20';
        }
        $picking->x_coordinate = $request->get('x_coordinate');
        $picking->y_coordinate = $request->get('y_coordinate');
        if ($picking->save()) {
            $response = ["register" => true];
        } else {
            $response = ["register" => false];
        }
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRequest(Request $request) {
        $picking = Picking::where('code', '=', $request->get('code'))
                ->first();
        $picking->x_coordinate = $request->get('x_coordinate');
        $picking->y_coordinate = $request->get('y_coordinate');
        $picking->save();
        $requestPicking = \App\Models\Request::whereDoesntHave('response')->first();
        if ($requestPicking) {
            $responsePicking = new \App\Models\Response();
            $responsePicking->request_id = $requestPicking->id;
            $responsePicking->picking_id = $picking->id;
            $responsePicking->estimated_time = '00:10:00';
            $responsePicking->arrived = 0;
            if ($responsePicking->save()) {
                $response = [
                    "response" => true,
                    'data' => [
                        "response_id" => $responsePicking->id,
                        "x_coordinate" => $requestPicking->x_coordinate,
                        "y_coordinate" => $requestPicking->y_coordinate,
                    ]
                ];
            } else {
                $response = [
                    "response" => false,
                    'data' => []
                ];
            }
        } else {
                $response = [
                    "response" => false,
                    'data' => []
                ];
            }
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function arrive(Request $request) {
        $responsePicking = \App\Models\Response::find($request->get('response'));
        $responsePicking->arrived = 1;
        if ($responsePicking->save()) {
            $response = ["response" => true];
        } else {
            $response = ["response" => false];
        }
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getResponse(Request $request) {
        $responsePicking = \App\Models\Response::find($request->get('response'));
        if ($responsePicking->shipment) {
            $response = [
                "shipment" => true,
                'data' => [
                    "shipment_id" => $responsePicking->shipment->id,
                    "x_coordinate" => $responsePicking->shipment->area->x_coordinate,
                    "y_coordinate" => $responsePicking->shipment->area->y_coordinate,
                ]
            ];
        } else {
            $response = [
                "shipment" => false,
                'data' => []
            ];
        }
        return json_encode($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function finish(Request $request) {
        $shipment = \App\Models\Shipment::find($request->get('shipment'));
        $shipment->complete = 1;
        if ($shipment->save()) {
            $response = ["response" => true];
        } else {
            $response = ["response" => false];
        }
        return json_encode($response);
    }

}
