<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Settings;
use \Auth;

class HomeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings() {
        $settings = Settings::all();
        return view('home.settings', ['settings' => $settings]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSettings(Request $request) {
        $settings = Settings::all();
        foreach ($settings as $setting) {
            $rules = [
                $setting->name => 'string|max:191',
            ];
        }
        $this->validate($request, $rules);
        $data = $request->all();
        foreach ($settings as $setting) {
            $setting->value = $data[$setting->machine_name]?$data[$setting->machine_name]:$setting->value;
            $setting->save();
        }
        return redirect('/settings');
    }

}
