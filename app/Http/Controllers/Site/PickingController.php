<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Settings;
use \Auth;

class PickingController extends Controller {
    
    public function index() {
        $user = Auth::user();
        $requestPicking = \App\Models\Request::where('user_id', '=', $user->id)
                ->where('current', '=', 1)
                ->first();
        if (!$requestPicking) {
            return view('picking.request');
        } else {
            if (!$requestPicking->response) {
                return view('picking.pending', ['requestPicking' => $requestPicking]);
            } else {
                if ($requestPicking->response->arrived) {
                    $areas = \App\Models\Area::all();
                    return view('picking.response', ['requestPicking' => $requestPicking, 'areas' => $areas]);
                } else {
                    return view('picking.wait', ['requestPicking' => $requestPicking]);
                }
            }
        }
        return view('picking.request');
    }

    public function store(Request $request) {
        $requestPicking = new \App\Models\Request();
        $requestPicking->x_coordinate = $request->get('x_coordinate');
        $requestPicking->y_coordinate = $request->get('y_coordinate');
        $requestPicking->current = 1;
        $requestPicking->user_id = Auth::user()->id;
        $requestPicking->save();
        return redirect('/picking/request');
    }
    
    public function update(Request $request) {
        $user = Auth::user();
        $requestPicking = \App\Models\Request::where('user_id', '=', $user->id)
                ->where('current', '=', 1)
                ->first();
        $response = $requestPicking->response;
        $requestPicking->current = 0;
        $shipment = new \App\Models\Shipment();
        $shipment->description = $request->get('description');
        $shipment->estimated_time = '00:10:00';
        $shipment->area_id = $request->get('area');
        $response->shipment()->save($shipment);
        $requestPicking->save();
        return redirect('/picking/request');
    }
    
    public function listIndex() {
        $user = Auth::user();
        $requests= \App\Models\Request::where('user_id', '=', $user->id)
                ->where('current', '=', 0)
                ->get();
        return view('picking.list',['requests'=>$requests]);
    }

}
