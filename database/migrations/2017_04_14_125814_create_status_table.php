<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('current');
            $table->timestamps();
            $table->integer('picking_id')->unsigned();
            $table->integer('status_type_id')->unsigned();

            $table->foreign('picking_id')->references('id')->on('picking')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_type_id')->references('id')->on('status_type')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
