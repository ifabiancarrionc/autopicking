<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->time('estimated_time');
            $table->timestamps();
            $table->integer('response_id')->unsigned();
            $table->integer('area_id')->unsigned();

            $table->foreign('response_id')->references('id')->on('response')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('area')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment');
    }
}
