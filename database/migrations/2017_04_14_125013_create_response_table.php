<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('response', function (Blueprint $table) {
            $table->increments('id');
            $table->time('estimated_time');
            $table->timestamps();
            $table->integer('request_id')->unsigned();
            $table->integer('picking_id')->unsigned();
            $table->tinyInteger('arrived');

            $table->foreign('request_id')->references('id')->on('request')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('picking_id')->references('id')->on('picking')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('response');
    }
}
