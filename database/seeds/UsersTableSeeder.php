<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => "Admin",
            'email' => "admin@autopicking.com",
            'password' => \Hash::make('4dm4ut0Pick1ng!!'),
            'active' => 1,
            'api_token' => str_random(60),
            'created_at' => new Datetime,
            'updated_at' => new Datetime,
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => "Picking",
            'email' => "picking@autopicking.com",
            'password' => \Hash::make('&4ut0Pick1ng&'),
            'active' => 1,
            'api_token' => str_random(60),
            'created_at' => new Datetime,
            'updated_at' => new Datetime,
        ]);
        DB::table('users')->insert([
            'id' => 3,
            'name' => "Técnico",
            'email' => "tecnico@autopicking.com",
            'password' => \Hash::make('T3ch4ut0Pick1ng$$'),
            'active' => 1,
            'api_token' => str_random(60),
            'created_at' => new Datetime,
            'updated_at' => new Datetime,
        ]);
        DB::table('users')->insert([
            'id' => 4,
            'name' => "Operador",
            'email' => "operador@autopicking.com",
            'password' => \Hash::make('0p3rad0r4ut0Pick1ng$$'),
            'active' => 1,
            'api_token' => str_random(60),
            'created_at' => new Datetime,
            'updated_at' => new Datetime,
        ]);
    }
}
