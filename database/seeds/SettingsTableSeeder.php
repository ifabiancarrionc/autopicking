<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'id' => 1,
            'name' => "Coordenada X Inicio",
            'machine_name' => "x_coordinate_begin",
            'type' => "text",
            'value' => '4.622828',
        ]);
        DB::table('settings')->insert([
            'id' => 2,
            'name' => "Coordenada Y Inicio",
            'machine_name' => "y_coordinate_begin",
            'type' => "text",
            'value' => '-74.130699',
        ]);
        DB::table('settings')->insert([
            'id' => 3,
            'name' => "Coordenada X Fin",
            'machine_name' => "x_coordinate_end",
            'type' => "text",
            'value' => '4.622524',
        ]);
        DB::table('settings')->insert([
            'id' => 4,
            'name' => "Coordenada X Fin",
            'machine_name' => "y_coordinate_end",
            'type' => "text",
            'value' => '-74.130465',
        ]);
        DB::table('settings')->insert([
            'id' => 5,
            'name' => "Mapa",
            'machine_name' => "map",
            'type' => "file",
            'value' => 'mapa.png',
        ]);
    }
}
