<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => "admin",
            'display_name' => 'Administrador'
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => "picking",
            'display_name' => 'Picking'
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'name' => "tecnico",
            'display_name' => 'Técnico'
        ]);
        DB::table('roles')->insert([
            'id' => 4,
            'name' => "operador",
            'display_name' => 'Operador'
        ]);
    }
}
