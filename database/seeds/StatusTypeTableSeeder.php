<?php

use Illuminate\Database\Seeder;

class StatusTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_type')->insert([
            'id' => 1,
            'name' => "Disponible",
            'description' => 'Disponible',
            'machine_name' => 'primary'
        ]);
        DB::table('status_type')->insert([
            'id' => 2,
            'name' => "Ocupado",
            'description' => 'Ocupado',
            'machine_name' => 'success'
        ]);
        DB::table('status_type')->insert([
            'id' => 3,
            'name' => "No Disponible",
            'description' => 'No Disponible',
            'machine_name' => 'warning'
        ]);
        DB::table('status_type')->insert([
            'id' => 4,
            'name' => "Falla",
            'description' => 'Falla',
            'machine_name' => 'danger'
        ]);
        DB::table('status_type')->insert([
            'id' => 5,
            'name' => "Fuera de Operación",
            'description' => 'Fuera de Operación',
            'machine_name' => 'danger'
        ]);
    }
}
