<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
Route::group(['middleware' => 'auth:api', 'prefix' => 'v1'], function () {
    Route::get('/picking/register', 'Api\PickingController@register');
    Route::get('/picking/request', 'Api\PickingController@getRequest');
    Route::get('/picking/arrive', 'Api\PickingController@arrive');
    Route::get('/picking/response', 'Api\PickingController@getResponse');
    Route::get('/picking/finish', 'Api\PickingController@finish');
});
