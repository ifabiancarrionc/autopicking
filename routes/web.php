<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::get('/password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/forgot-password', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('/', 'Auth\LoginController@login');
    Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/user/profile', 'Site\UserController@profile');
    Route::post('/user/profile', 'Site\UserController@profileUpdate');
    Route::get('/picking/request', 'Site\PickingController@index');
    Route::post('/picking/request', 'Site\PickingController@store');
    Route::post('/picking/response', 'Site\PickingController@update');
    Route::get('/picking', 'Site\PickingController@listIndex');
});
Route::group(['middleware' => 'role:admin'], function () {
    Route::resource('/user', 'Site\UserController');
    Route::get('/settings', 'Site\HomeController@settings');
    Route::post('/settings', 'Site\HomeController@storeSettings');
});
Route::group(['middleware' => 'role:tecnico|admin'], function () {
});
